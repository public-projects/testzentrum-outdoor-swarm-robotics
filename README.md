# Testzentrum Outdoor Swarm Robotics

This is a public project to distribute information about the AG "Outdoor Swarm Robotics" of Silicon Saxony. 

## Objective
* Form a sub-working-group of AG Lausitz of Silicon Saxony for "AI algorithms for Outdoor and Swarm Robotics"
* Initiate a test center for outdoor swarms in the Lausitz.
    * Indoor 100qm test hall
    * Outdoor parcours and area for testing applications

## Communication
* Mailing list: https://groups.google.com/g/testzentrum-outdoor-swarm-robotics
* Blog page:    https://md.inf.tu-dresden.de/blog-testzentrum-outdoor-swarm-robotics
* Gitlab document portal:  https://git-st.inf.tu-dresden.de/public-projects/testzentrum-outdoor-swarm-robotics
