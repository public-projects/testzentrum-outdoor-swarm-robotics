Lieber XX,

Herzliche Einladung zur Mailingliste für die Akquise und Lobbying zum
"Outdoor Swarm Robotics" Außenlabor in der Lausitz. Dies ist eine Aktion
der AG Lausitz von Silicon Saxony. 

Unter https://git-st.inf.tu-dresden.de/public-projects/testzentrum-outdoor-swarm-robotics/
finden Sie ein neues öffentliches Dokumentenportal der Unter-AG "AI for Outdoor and Swarm Robotics".
Weitere Informationen über gemeinsame Termine, Aktivitäten zur die Initiative zum Testzentrum 
in der Lausitz finden Sie unter:
* https://git-st.inf.tu-dresden.de/public-projects/testzentrum-outdoor-swarm-robotics/

* Pitch der UAG: https://git-st.inf.tu-dresden.de/public-projects/testzentrum-outdoor-swarm-robotics/-/blob/master//UAG-Lausitz-ai-for-outdoor-swarm-robotics-pitch.pdf

* Pitch der Initiative Testzentrum https://git-st.inf.tu-dresden.de/public-projects/testzentrum-outdoor-swarm-robotics/-/blob/master//test-center-outdoor-swarm-robotics-pitch.pdf

Siehe die andere Mails auf der Mailingliste. 

Gruß
Uwe Aßmann
