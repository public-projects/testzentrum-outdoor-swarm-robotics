# Blog für Testzentrum "Test Center Outdoor and Swarm Robotics” (TECOSAR)

* Mailingliste https://groups.google.com/g/testzentrum-outdoor-swarm-robotics

**SYNCTAG: [This Blog page can be edited by all members of the TECOSAR Mailing list.](https://md.inf.tu-dresden.de/blog-testzentrum-outdoor-swarm-robotics?both )**

## Aktuelles To Do
* Nächster Termin Do 20.5. 9:00
	* Ort: wird von Steve Federow bekannt gegeben
* Goal "Robot Valley Saxony" verankern
* Federow: Sensory - Fallstudie

## SAS Meeting 2021-07-22

Teilnehmer: Frau Reinisch, SAS, Bauingenieur) Dr. Bryja, SMWK; Dr. Poetschke SMWA (Referat Technologie), Norbert Wenke (SAS Geschäftsführer)

* SAS macht Kommunenberatung - Arbeitsplätze müssen flankiert werden

* Dr Bryja: Projektförderung im Grundsatzreferat

* N. Wenke: Techniker+Betriebswirt, Kreislaufwirtschaft, Beirat Zentrum Telematik+Robotik Würzburg

### Resultate

* Strukturmittel aus der Braunkohleförderung nur für Kommunen, kommunale Unternehmen - kommunale Orgs, nur investiv einzusetzen

* Kann man Personal über STARK fördern? 

* Personalgelder können gefördert werden über InnoPro, TG70 (Nur Hochschulen), Innovatives Europa (nur Forschung, z.B. InnoTeams)
* Finanzierung des Initialprojektes ist über Maßnahmen des SMWA möglich
  * InitialProjekt ist alleine "lebensfähig", auch ohne Braunkohleförderung
  * Momentan werden Förderprogramme im SWMA finanziell abgestimmt, das sieht ganz gut aus 
  * Anträge 2022 auf InnoTeams sind möglich. 
    * Ob alle Cases unter einem Dach zu bringen sind, wäre zu diskutieren
  * Operationeller Antrag stellen im Q1/22, Genehmigung Mitte 2022

### Betriebsphase

* Betreibung muss für 12 Jahre zugesichert werden, sonst müssen die Fördergelder zurückgezahlt werden
  * Zertifizierungsgelder sind Einnahmen

* Antrag auf Infrastruktur kann durch Kommune gestellt werden
  * Betreiber muss diskriminierungsfrei ausgeschrieben werden, Fördersatz muss bestimmt werden
  * Die GmbH AEF könnte sich in dieser Ausschreibung bewerben
  * Kommune vermietet an Firma zum Betrieb
  * Für Flughafen Kamenz, der Stadt und Landkreis gehört, müssen diese das wollen.
* Alternativ kann GAW-Infra-Antrag für klassische Technologiezentren durch Kommune  beantragt werden (90% förderquote)
* Wie kann man Flugplatz Rothenburg einbinden?
  * Kontakte bestehen durch AEF

### Maßnahmen

* Workshop in Kamenz mit Stadt organisieren, um Betriebskonzept zu planen (AEF)
* Wann anfangen, Anträge schreiben? Q4/2 und Q1/22
  * Im Kontakt bleiben
  * möglichst im Q4 Antragstemplates bestellen
  * Zweiter Fördertopf Kommunal beginnt im November. Bis Ende 2021 müsste man mit dem Konzept für Investitionen für das Testzentrum klar sein, 2022 Juni ist Entscheidung
* Ansiedlung von solidStateAI durch WFS begleiten lassen (Federow, Aßmann)
* Diskussion über Aufteilung der Cases auf verschiedene Flugplätze führen
  * Rothenburg - Recyclingrobotik (Verbundstoffe)
  * Hoyerswerda/Nardt - (Re-)kultivierungsrobotik
  * Kamenz: Katastrophenschutz-Robotik, Assistenzrobotik
  * Ist ein Übergreifendes Betreibermodell möglich? Dazu wäre Landkreisverbund wäre nötig.


## 2021-07-01 Meeting
* Teilnehmer: Krzywinski, Ernstberger AEF, 
* Agenda:
	* Infos von Jonas Schreiber (Wandelbots)
		* Koordination über SAS wg. Projektvorstellung und -abstimmung
		* 22.07 um 15 Uhr (Frau Kühne, SAS, Blasewitzer Straße 82, 01307 Dresden) NOCH TENTATIV, ggf alternativ
		* Pfeiffer, Federow, Hardt, Schuster/NuP, Aßmann, Schreiber 
		* Erwartungshaltung: SAS ist nicht für Inhalt zuständig, aber für Finanzierungsstruktur
		* Innoteams? SAB Verbundprojekte?
	* Folienstruktur
* Neue Mitglieder Mailingliste?
	* Hardt, WAKU Mitglied?
	* Trumpf
	* Franz Lehmann <Franz.Lehmann@trumpf.com>, Andreas Hultsch <andreas.hultsch@trumpf.com>, 
	* ABB (via AEF) - Lackierungsrobotik Diskutieren



## 2021-05-06 Meeting UAG Outdoor and Swarm Robotics 
* IFranke, BSchuster, DGöhringer, APodlubne, JKrzywinski, JMarkmiller, UAßmann, GAkühn, Thomas Ernstberger (AEF), MBelov, SFederow, JSchreiber, OGehrels, TSpringer, Sergio
* Neue Mitglieder: Kompetenzzentrum autonomes elektrisches Fliegen Kamenz (AEF), Flughafen Nardt (Hoy)
    * Drohnen-Flugpatz in Kamenz ist möglich
* Stand Skizze 
	* 6 Fallstudien
	* Priorisierung der Studien notwendig
	* Eventuell 1) und 3) verschmelzen? Zustimmung
* Ideal wäre Inno-Team Förderung. Seit 6.5. gibt es wieder EU-Gelder, die ggf. genutzt werden könnten (Federow fragt SMWA an)
	* Alte Richtlinie: https://www.sab.sachsen.de/f%C3%B6rderprogramme/sie-planen-ihre-mitarbeiter-oder-sich-selbst-weiterzubilden/innoteam.jsp
* Andere Richtlinien, die in Frage kommen:
	* Förderrichtlinien zum Strukturwandel in den Braunkohlegebieten. 
	* STARK vom BaFa https://www.bafa.de/DE/Wirtschafts_Mittelstandsfoerderung/Beratung_Finanzierung/Stark/stark_node.html 
	* StEP Revier vom SMR https://www.strukturentwicklung.sachsen.de/foerderrichtlinie-braunkohlereviere-4804.html
* Kontakt zu Droste und Gerels herstellen (Aßmann)
* Dr. Springer: Fa. Trumpf und Navigation (Franz Lehmann) (andreas.hultsch@trumpf.com)
* AEF: Sächsisches Lackierzentrum (ABB): Roboter drucken Lackierung
	* KI-Kompetenzzentrum (Schmauder) MMI, 
	* Wandelbots ist interessiert und hat Partner
	* Airbus Helikopter (Markmiller)
	* NuP
	* ggf. Usecase für ein neues Projekt zum Thema "AI and MR for Production" (Ingmar Franke redet mit S. Federow)

## 2021-04-28 Termin mit AEF
* Reith, Markmiller, Peter Pfeifer (AEF) und Thomas Ernstberger (AEF, Gefü), Aßmann, Federow
* Autonome Fahrzeuge im AEF sind geplant, Skalierung auf int. Flughäfen
	* Auotonome Rasenpflege
	* Pisteninspektion
	* Anschluss-flugtaxis, autonome Mobilität
* Alternative Antriebe (H2), elektrisches Fliegen
* AEF hatte Anfragen von RRoyce
* Hallenbau?
* Projekt?
* AEF will Testzentrum für Fliegen einrichten (Kooperationsvereinbarungen liegen vor mit Flugplatzeigner, Flurstück in der Nähe)
	* Stiftung Drohnentest
	* im Gespräch mit SAENA
* Könnte Parnter werden
https://www.pressebox.de/inaktiv/waku-robotics-gmbh/WAKU-Robotics-procurement-and-optimization-platform-for-mobile-robots-closes-first-financing-round/boxid/1018673

## UAG TECOSAR 2021-03-26 Protokollnotizen
Teilnehmer: Hardt, Gehrels, Federow, Schuster (NuP), Goehringer, Krzywinski, Polubne, Schreiber, Aßmann, Springer, Belov

* AG Lausitz und ihre Ziele
	* Abgabe 2-Seiter an Ministerien erfolgt
	* KI-Strategie Sachsen soll am 16.6. in Leipzig vorgestellt werden
* UAG Ziele
	* https://www.youtube.com/watch?v=6k5cN0nzpPI
* neue Interessenten: Mugler, WAKU robotics
* Vorstellung coboworx (Gehrels), NuP (Schuster)
* Vorstellung Maßnahmen STARK (BaFa) und STEP (SMR)
* Folien W. Hardt verteilen


# 2021-03-12 Protokollnotizen
Teilnehmer: Jonas Schreiber (Wandelbots); Uwe Aßmann, Diana Göhringer, Felix Schmitt, Mikhail Belov, Thomas Springer, Sebastian Ebert, Ariel Podlubne, Göhan Akgün (TUD); Wolfram Hardt (TU Chemnitz); Steve Federow (SiSax)
* Tetzlaff Termin nachfragen, mit Prorektor Götze TUC, 
	* Prof Jung (TU BA Freiberg)
* Jour Fixe Do 9-10 Uhr (Start ab 25.3.) alternierend zur SiSax-AG Lausitz 
* Gerne noch einen Pitch für Silicon Saxony Day einreichen  
    * zeitnah an steve.federow@silicon-saxony.de
    * https://www.silicon-saxony-day.de/
* und am Silicon Saxony Digital Happy Hour 25.3. (Impulsvortrag Prof. Hardt)
