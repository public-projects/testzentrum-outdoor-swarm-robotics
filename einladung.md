
Sehr geehrte Damen und Herren, 

wir lernten uns zum Thema "Cobotic" kennen. Seit einigen Monaten arbeitet der 
Silicon Saxony an einer "Arbeitsgruppe Lausitz", die jetzt 3 große Themengebiete festgelegt hat: 

1. „KI-Algorithmen für Outdoor Robotics" (Leitung: Prof. Uwe Aßmann, TUD)
2. „KI-Algorithmen für Digitale Bildgebung und -verarbeitung" (Leitung: Prof. Ehrenfried Zschech, Fraunhofer IKTS)
3. „KI-Algorithmen für dezentrale Energielösungen" (Leitung: Jens Scheibe, Bether)

Ich darf Sie hiermit einladen, sich über die Initiative 1) zu informieren, in dem Sie diese Mailingliste abonnieren. Infos über gemeinsame Termine, aktivitäten, die Initiative zum Testzentrum in der Lausitz finden Sie unter:
 https://git-st.inf.tu-dresden.de/public-projects/testzentrum-outdoor-swarm-robotics/

mit freundlichen Grüßen 

Ihr 
Uwe Aßmann
uwe.assmann@tu-dresden.de
Wiss. Beirat "Software" des Silicon Saxony

# Outdoor and Swarm Robotics (OSR) 

Folgende Robotik-Technologien werden sich bis zum Jahr 2030 stark entwickeln und dazu KI-Algorithmen benötigen:
* Search-and-Rescue (SAR) Robotics: Wie können Roboterschwärme im Katastrophenfall helfen, Menschen aus gefährlichen Situationen zu retten oder herauszuhalten?
*  Flood Management Robotics: Wie können Roboterschwärme im Flutfall helfen, Sicherungsdämme und -einrichtungen schnell aufzubauen?
* Corona-Bots: Wie können Roboterschwärme zu Sicherheits- und Kontrollmaßnahmen bei Pandemien eingesetzt werden?
*  Swarm Care Robotics: Wie können Roboterschwärme alten und behinderten Menschen in ihrem Leben zuhause, beim Einkaufen und in der Gesellschaft helfen?
*  Farming Robotics: Wie können Roboterschwärme bei der Feldarbeit helfen?
*  Recycling Robotics: Wie können Roboterschwärme bei der Zerlegung von komplexen Produkten zum Zwecke des besseren Recyclings helfen (Dekonstruktionsrobotik für Autos, Flugzeuge, Anlagen)?
*  Recultivation Robotics: Wie können Roboterschwärme bei der Rekultivierung von Brachflächen, die aus dem Braunkohletagebau stammen, helfen?
*  Quantum Swarm Robotics (Swarm robots with quantum nets, beyond von-Neumann): Wie können Roboterschwärme mit verteilten Gehirnen gebaut werden (Memristornetze, Quantum-bit-Netze), die die von-Neumann-Architekturen für Roboterschwärme überwinden?

Um neuartige Anwendungen durch diese Technologien zu entwickeln und die daraus entstehenden Märkte zu erschließen, ist es nötig, Hardware und Software der Anwendungen intensiven Tests in einer Laborumgebung zu unterziehen, in gemeinschaftlicher Zusammenarbeit zwischen Forschern und Industrie. Eine solche Laborumgebung sollte allerdings bei „Outdoor und Swarm Robotics" aus einer Halle mit einem Testaußengelände bestehen.

Ziele

Zentrale Ziele der Unterarbeitsgruppe der Lausitz-AG sind: 
* Unterstützung eines Antrags auf ein Testzentrum in der Region Lausitz, z.B. Hoyerswerda/Kamenz, mit Halle und Gelände eines Außenlabors, in dem Forschung, Industrie und Anwendungsfirmen zusammenarbeiten
* Mitwirkung an der Erarbeitung einer InnoTeam-Förderrichtlinie im Rahmen der Braunkohle-Initiative, um „Zellen" für Forschung und anschließenden Transfer oder Gründung vorzubereiten („Kopftransfer")
* Planung der Zusammenarbeit mit dem SCADS.AI-Zentrum der TU Dresden/U Leipzig, sowie dem Exzellenzcluster „Center for Tactile Internet (CeTI)" der TU Dresden

