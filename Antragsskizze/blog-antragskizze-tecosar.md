# Antragsskizze TECOSAR
**SYNCTAG: edit this only in [codiMD](https://md.inf.tu-dresden.de/antragsskizze-tecosar), not in git-st-lab.**

## Schlesinger: Gewerbepark Kühnicht, Hoy
* 68x24qm Halle vorhanden
	* Bauen40 ist Konkurrenz
	* Baumaschinen ist gegenüber
* Außengelände ca. 500qm
* Knappenroder Energiefabrik
* Rekultivierungsrobotik LEAG

# Information für das SMWA (Gekürzte Form)
* Gesamtziel des Vorhabens – Welches Problem soll gelöst werden?
* Stand der Wissenschaft und Technik
* Arbeitsziele des Vorhabens und vorgesehene Lösungswege
* Erfolgsaussichten
* Verwertungspotenzial
* Beiträge des Vorhabens zum Strukturwandel
* Kompetenz der Antragsteller
* Überschläge Kostenaufstellung (einmalig/dauerhaft)

## Gesamtziel des Vorhabens – Welches Problem soll gelöst werden?
## Stand der Wissenschaft und Technik
## Arbeitsziele des Vorhabens und vorgesehene Lösungswege
## Erfolgsaussichten
## Verwertungspotenzial
## Beiträge des Vorhabens zum Strukturwandel
## Kompetenz der Antragsteller
## Überschläge Kostenaufstellung (einmalig/dauerhaft)


# Vernetzung zu den anderen Anträgen

* Energie und Green Robotics
* Bildverarbeitung


# Mail von SMWA

Wir haben uns in einer ersten TelKo im SMWA gemeinsam mit der Sächsischen Agentur für Strukturentwicklung (SAS) zu Ihren drei kurz skizzierten Ideen ausgetauscht. Wir fanden alle drei Ideen interessant, wenn auch die sofort erkennbare „Nähe“ zur Lausitz verschieden ausgeprägt scheint.

Wir erbitten vor einer weiteren Befassung mehr Informationen zu den drei Projekten, die dann zu einer jeweils ersten Diskussion genutzt werden sollen.

Für die etwas erweiterten Beschreibungen (Idee: Pro Projektidee max. zwei Seiten) sind durch unsere Förderexperten folgende Informationen erbeten:

# Ausführlicher Antrag
* Ansprechpartner für das jeweilige Projekt
* Gesamtziel des Vorhabens – Welches Problem soll gelöst werden?
* Angabe der investiven und konsumtiven Projektkosten
* Wer soll Antragsteller für das Projekt werden?
* Stand der Wissenschaft und Technik
  * knappe fachliche Beschreibung zum Stand der Wissenschaft und Technik zum Zeitpunkt der Antragstellung
  * Warum genügt der Stand der Wissenschaft und Technik nicht zur Lösung des Problems?
  * Angabe von anderen Lösungen
  * Darstellung der Schutzrechtssituation (eigene und fremde Schutzrechte)
* Wissenschaftlich-technische Arbeitsziele des Vorhabens und vorgesehene Lösungswege
  * Erläuterung der mit dem Vorhaben angestrebten wissenschaftlichen und technischen Arbeitsziele im Vergleich zum Stand der Technik (eventuell tabellarisch)
  * vorgesehene Lösungswege zur Erreichung der Vorhabensziele im Vergleich zum Stand der Technik
  * Darstellung des Neuheitsgrades im nationalen und internationalen Maßstab
  * Angabe der zu erreichenden Verfahrens- oder Produktparameter, Eigenschaften, Funktionen usw.
  * Angabe möglicher Schutzrechtsanmeldungen
* Wissenschaftlich-technische Erfolgsaussichten
  * Einschätzung der Erreichbarkeit der wissenschaftlichen Ziele
  * Aufwendungen nach Ende des Vorhabens bis zur Erreichung eines umsatzwirksamen Arbeitsstandes
* Verwertungspotenzial der Entwicklung
  * Marktanalyse/-abschätzung für die Vorhabensergebnisse (potentielle Marktanteile, Kunden, Produkte, Stückzahlen, Preise, Umsätze, erwartete Nutzungsdauer der Vorhabensergebnisse in Jahren)
  * Verwertungspotenzial der Vorhabensergebnisse für den Antragsteller selbst und für den Freistaat Sachsen (Schaffung neuer Arbeitsplätze, unternehmensübergreifende Effekte z. B. bei Lieferanten, Kooperationspartnern usw.)
* Beiträge des Vorhabens zum Strukturwandel (Bitte gehen Sie auf die folgenden Punkte konkret ein: Anzahl der erhaltenen/geschaffenen Arbeits-/Ausbildungsplätze; Diversifizierung der Wirtschaftsstruktur und Verbesserung der Attraktivität der Wirtschaftsstandorte)
* Kompetenz der Antragsteller / Projektpartner
* Überschläge Kostenaufstellung (einmalig/dauerhaft) (Können die dauerhaften Kosten, bspw. Unterhaltung usw. eigenständig aufgebracht werden?)

# Gekürzte Information
* Gesamtziel des Vorhabens – Welches Problem soll gelöst werden?
* Stand der Wissenschaft und Technik
* Arbeitsziele des Vorhabens und vorgesehene Lösungswege
* Erfolgsaussichten
* Verwertungspotenzial
* Beiträge des Vorhabens zum Strukturwandel
* Kompetenz der Antragsteller
* Überschläge Kostenaufstellung (einmalig/dauerhaft)

## Vernetzung zu den anderen Anträgen

* Energie und Green Robotics
* Bildverarbeitung