# SAS Meeting 2021-07-22

Teilnehmer: Frau Reinisch, SAS, Bauingenieur) Dr. Bryja, SMWK; Dr. Poetschke SMWA (Referat Technologie), Norbert Wenke (SAS Geschäftsführer)

* SAS macht Kommunenberatung - Arbeitsplätze müssen flankiert werden

* Dr Bryja: Projektförderung im Grundsatzreferat

* N. Wenke: Techniker+Betriebswirt, Kreislaufwirtschaft, Beirat Zentrum Telematik+Robotik Würzburg

## Resultate

* Strukturmittel aus der Braunkohleförderung nur für Kommunen, kommunale Unternehmen - kommunale Orgs, nur investiv einzusetzen

* Kann man Personal über STARK fördern? 

* Personalgelder können gefördert werden über InnoPro, TG70 (Nur Hochschulen), Innovatives Europa (nur Forschung, z.B. InnoTeams)
* Finanzierung des Initialprojektes ist über Maßnahmen des SMWA möglich
  * InitialProjekt ist alleine "lebensfähig", auch ohne Braunkohleförderung
  * Momentan werden Förderprogramme im SWMA finanziell abgestimmt, das sieht ganz gut aus 
  * Anträge 2022 auf InnoTeams sind möglich. 
    * Ob alle Cases unter einem Dach zu bringen sind, wäre zu diskutieren
  * Operationeller Antrag stellen im Q1/22, Genehmigung Mitte 2022

## Betriebsphase

* Betreibung muss für 12 Jahre zugesichert werden, sonst müssen die Fördergelder zurückgezahlt werden
  * Zertifizierungsgelder sind Einnahmen

* Antrag auf Infrastruktur kann durch Kommune gestellt werden
  * Betreiber muss diskriminierungsfrei ausgeschrieben werden, Fördersatz muss bestimmt werden
  * Die GmbH AEF könnte sich in dieser Ausschreibung bewerben
  * Kommune vermietet an Firma zum Betrieb
  * Für Flughafen Kamenz, der Stadt und Landkreis gehört, müssen diese das wollen.
* Alternativ kann GAW-Infra-Antrag für klassische Technologiezentren durch Kommune  beantragt werden (90% förderquote)
* Wie kann man Flugplatz Rothenburg einbinden?
  * Kontakte bestehen durch AEF

## Maßnahmen

* Workshop in Kamenz mit Stadt organisieren, um Betriebskonzept zu planen (AEF)
* Wann anfangen, Anträge schreiben? Q4/2 und Q1/22
  * Im Kontakt bleiben
  * möglichst im Q4 Antragstemplates bestellen
  * Zweiter Fördertopf Kommunal beginnt im November. Bis Ende 2021 müsste man mit dem Konzept für Investitionen für das Testzentrum klar sein, 2022 Juni ist Entscheidung
* Ansiedlung von solidStateAI durch WFS begleiten lassen (Federow, Aßmann)
* Diskussion über Aufteilung der Cases auf verschiedene Flugplätze führen
  * Rothenburg - Recyclingrobotik (Verbundstoffe)
  * Hoyerswerda/Nardt - (Re-)kultivierungsrobotik
  * Kamenz: Katastrophenschutz-Robotik, Assistenzrobotik
  * Ist ein Übergreifendes Betreibermodell möglich? Dazu wäre Landkreisverbund wäre nötig.

