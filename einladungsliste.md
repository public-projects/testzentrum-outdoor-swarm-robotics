# Mitglieder, OK
* NuP (Björn Schuster)
* Coboworx: 	Olaf Gerels,  AP: Klaus Wagner    https://coboworx.com/ 	
* Mugler (Dr. Dod, via Fiedler)
* Wandelbots: –	https://wandelbots.com/de/ –	AP: Jonas Schreiber
* WAKU robotics Alexander Bresk 0151 212 266 29 offen
* Bosch Sensortec

* Hardt

## Einzutragend
* Tobias Zerger <tobias.zerger@tu-dresden.de>
* Felix Schmitt <felix.schmitt_1@tu-dresden.de>

# Einzuladende in Liste

## Ongoing
* Fabmatics: –	https://www.fabmatics.com/de/ –	AP: Martin Däumler
* Security Robotics Leipzig https://security-robotics.de/en/wheel-driven-robots.html

## Akademie
* Janschek, Beitelschmidt
* Fricke ?
* Reichelt
* Jung (Freiberg)
* BASELABS
* Kamenzer Kontakt AEF Aero
* EU-Fördertöpfe mit Breslau können genutzt werden
    * CASUS: Lindner vermittelt Kontakt auf Dienstebene
    * Modellregion Lausitz für Zusammenarbeit in Europa

### VLUFFI
* Prof. Weber, Baumaschinen
* Braune	Annerose	EuI, Automatisierungstechnik
* Wollschlaeger	Martin	IAI, Prozesskommunikation
* Altinsoy	Ercan	IAS, Akustik und Haptik
* Otto Christoph	Inst.f.Naturstofftechnik, Bioverfahrenstechnik
* Schmauder 	Martin	Arbeitswissenschaft
* Tiepmar	Jonas	Maker Space/ SLUB
* Lemme	Gordon	
* Ihlenfeldt	Steffen	IMD/ LWM
* Tetzlaff	Ronald	IEE/ EuI
* Stelzer	Ralf	MW/ KTC
* Röstel	Holger	BING
* Loeper 	Michael	ETIT Werkstatt


## Fördermöglichkeiten
* SMWA: Verbundprojekte
* Stark-Richtlinie

### Unklar
* Struktur im Revier (f+E)
* Innovationsrichtlinie Transfer (Nothnagel, SMWA)

## Kontakte
* SK: Kollege von Lindner: Martin Eppschner
* SMR: Milan Patek
* SMEKUL: Dr. Geisler

## Andere
* VTG der med. Fak. TUD
      * https://www.uniklinikum-dresden.de/de/das-klinikum/kliniken-polikliniken-institute/vtg
      * Prof. Jürgen Weitz
      * Begründung: Einsatz von MR und Robotik bei der OP, Visite
* Klinik für Inneres der med. Fak. TUD
      * https://www.uniklinikum-dresden.de/de/das-klinikum/kliniken-polikliniken-institute/zim
      * AP: Prof. Jochen Hampe
      * Begründung: Einsatz von MR/Robotik für Untersuchung, Therapie, Visite



Herzlich willkommen auf der Liste "Testzentrum Outdoor und Swarm Robotics" von Silicon Saxony. 

Mailarchiv unter https://groups.google.com/g/testzentrum-outdoor-swarm-robotics/
Gruß

Uwe Aßmann
